################################################################################
# Variables
################################################################################
ROOT_DIR            := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BOLD                := $(shell tput bold)
BLACK        		:= $(shell tput -Txterm setaf 0)
RED         		:= $(shell tput -Txterm setaf 1)
GREEN       		:= $(shell tput -Txterm setaf 2)
YELLOW       		:= $(shell tput -Txterm setaf 3)
LIGHTPURPLE  		:= $(shell tput -Txterm setaf 4)
PURPLE       		:= $(shell tput -Txterm setaf 5)
BLUE         		:= $(shell tput -Txterm setaf 6)
WHITE        		:= $(shell tput -Txterm setaf 7)
RESET  				:= $(shell tput -Txterm sgr0)

DOCKER              := docker
DOCKER_COMPOSE      := docker-compose
DOCKER_COMPOSE_FILE := docker-compose.yml
# Change this if your .env is in a custom directory
DOCKER_ENV_FILE     := .docker/.env
DOCKER_COMPOSE_ENV  := --env-file $(DOCKER_ENV_FILE)

CONTAINERS_BASICS   := traefik certs portainer
CONTAINERS_MYSQL    := $(CONTAINERS_BASIC) mysql redis phpmyadmin
CONTAINERS_POSTGRES := $(CONTAINERS_BASIC) postgres redis pgadmin
CONTAINERS_ELK      := $(CONTAINERS_BASIC) elasticsearch kibana
CONTAINERS_TOOLS    := $(CONTAINERS_BASIC) maildev

# Include docker .env constant
include $(DOCKER_ENV_FILE)
export $(shell sed 's/=.*//' $(DOCKER_ENV_FILE))

.DEFAULT_GOAL    	:= help
HELP_FUN 			:= \
    %help; \
    while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3, $$4] if /^([a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
    print "usage: make [target]\n\n"; \
    for (sort keys %help) { \
    print "${BOLD}${WHITE}$$_:${RESET}\n"; \
    for (@{$$help{$$_}}) { \
		$$sep = " " x (24 - length $$_->[0]); \
		print "  ${YELLOW}$$_->[0]${RESET}$$sep${LIGHTPURPLE}$$_->[1]${RESET}\n"; \
		print "$$_->[2]"; \
    }; \
    print "\n"; }

.PHONY: help dependencies show up start stop restart status ps clean bash

################################################################################
# Helpers
################################################################################
confirm: ##@other Used for validate some commands.
	@( read -p "$(RED)Are you sure? [y/N]$(RESET): " sure && case "$$sure" in [yY]) true;; *) false;; esac )

help: ##@other Show this help.
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

################################################################################
# Debug Variables
################################################################################
colors: ##@debug Show all the colors available makefiles
	@echo "${BOLD}BOLD${BOLD}"
	@echo "${BLACK}BLACK${RESET}"
	@echo "${RED}RED${RESET}"
	@echo "${GREEN}GREEN${RESET}"
	@echo "${YELLOW}YELLOW${RESET}"
	@echo "${LIGHTPURPLE}LIGHTPURPLE${RESET}"
	@echo "${PURPLE}PURPLE${RESET}"
	@echo "${BLUE}BLUE${RESET}"
	@echo "${WHITE}WHITE${RESET}"

show: ##@debug Show make constant c=<name>
	@echo $(c) = $($(c))

################################################################################
# Docker
################################################################################
bash: ##@docker Access to c=<name> container bash
	@$(DOCKER) exec -it $(c) bash

clean: confirm ##@docker Clean all data
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) down --remove-orphans

exec: ##@docker Access to cont=<name> container and execute cmd=<name> command
	@$(DOCKER) exec -it $(cont) $(cmd)

ip: ##@docker Get IP of the c=<name> container
	@$(DOCKER) inspect -f '{{range.NetworkSettings.Networks}} {{.IPAddress}}{{end}}' $(c)

ips: ##@docker Get IP of all containers
	@$(DOCKER) ps -q | xargs -n 1 $(DOCKER) inspect --format '{{ .Name }} :{{range.NetworkSettings.Networks}} {{.IPAddress}}{{end}}' | sed 's#^/##'

kill: confirm ##@docker Clean all data
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) down
	@$(DOCKER) rm -f $($(DOCKER) ps -a -q)
	@$(DOCKER) volume rm $($(DOCKER) volume ls -q)
	@$(DOCKER) system prune -a

logs: ##@docker Show logs for all or c=<name> containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) logs --tail 1000 -f $(c)

network: ##@docker Create a n=<name> network
	@$(DOCKER) network create $(n)

ps: status ##@docker Alias of status

status: ##@docker Show status of containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) ps

restart: stop start ##@docker Restart all or c=<name> containers

stop: ##@docker Stop all or c=<name> containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) stop $(c)

down: ##@docker Stop all or c=<name> containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) down $(c) --remove-orphans

start: ##@docker Start all or c=<name> containers in background
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) up -d --build $(c)

basics: ##@docker Start all $(CONTAINERS_BASICS) containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) up -d --build $(CONTAINERS_BASICS)

mysql: ##@docker Start all $(CONTAINERS_MYSQL) containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) up -d --build $(CONTAINERS_MYSQL)

postgres: ##@docker Start all $(CONTAINERS_POSTGRES) containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) up -d --build $(CONTAINERS_POSTGRES)

elk: ##@docker Start all $(CONTAINERS_ELK) containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) up -d --build $(CONTAINERS_ELK)

tools: ##@docker Start all $(CONTAINERS_TOOLS) containers
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) up -d --build $(CONTAINERS_TOOLS)

up: ##@docker up all or c=<name> containers in foreground
	@$(DOCKER_COMPOSE) $(DOCKER_COMPOSE_ENV) -f $(DOCKER_COMPOSE_FILE) up $(c)
