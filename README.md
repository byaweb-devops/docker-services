# Dockers services

## Getting started

Use makefile

```bash
    sudo apt-get update && sudo apt-get upgrade
    sudo apt-get install make
```

Launch containers

```bash
    # With Makefile
    make network docker-network
    # Without Makefile
    docker network create docker-network
    # With Makefile
    make start
    # Without Makefile
    docker-composer --env-file .docker/.env up -d
```
